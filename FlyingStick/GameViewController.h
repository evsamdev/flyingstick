//
//  GameViewController.h
//  FlyingStick
//

//  Copyright (c) 2014 EvSamsonov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "HomeViewController.h"
#import "GameScene.h"

@interface GameViewController : UIViewController<HomeViewControllerDelegate,GameSceneDelegate>

@property (nonatomic, strong) HomeViewController* homeViewController;
@property (nonatomic, strong) GameScene *scene;



@end
