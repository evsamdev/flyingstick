//
//  GameScene.m
//  FlyingStick
//
//  Created by Evgeny on 4/10/2014.
//  Copyright (c) 2014 EvSamsonov. All rights reserved.
//

#import "GameScene.h"
#import "LevelProvider.h"

@interface GameScene()

@property (nonatomic) NSInteger ballsCount;
@property (nonatomic) NSInteger levelIndex;
@property (nonatomic) NSInteger sticksCount;
@property (nonatomic) BOOL levelIsStarted;
@property (nonatomic) BOOL isStickFlying;
@property (nonatomic,strong) SKLabelNode *levelLabel;
@property (nonatomic,strong) NSNumber *lastLevel;

@end


@implementation GameScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    
    self.physicsWorld.gravity = CGVectorMake(0,0);
    
    
    NSLog(@"%f %f %f %f",CGRectGetMinX(self.frame), CGRectGetMaxX(self.frame) , CGRectGetMinY(self.frame) , CGRectGetMaxX(self.frame));
    
    SKLabelNode *levelLabel = [SKLabelNode labelNodeWithFontNamed:@"Maddac"];
    
    levelLabel.text = @"Level 1";
    levelLabel.fontSize = 30;
    levelLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                   CGRectGetMinY(self.frame)+10);
    
    [self addChild:levelLabel];
    
    
    SKLabelNode *menuLabel = [SKLabelNode labelNodeWithFontNamed:@"Maddac"];
    menuLabel.name = @"menuButtonNode";
    menuLabel.text = @"MENU";
    menuLabel.fontSize = 30;
    menuLabel.position = CGPointMake(CGRectGetMidX(self.frame)+180,
                                      CGRectGetMinY(self.frame)+10);
    
    [self addChild:menuLabel];
    
    
    self.levelLabel = levelLabel;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSNumber *lastLevel = [defaults objectForKey:@"lastLevel"];
    if(lastLevel){
        self.levelIndex = [lastLevel integerValue]-1;
    }else{
        self.levelIndex = 0;
    }
    [self initNextLevel];
   
}

-(void)resetLevel{
    
    NSLog(@"self.lastLevel: %@",self.lastLevel);
    if(!self.lastLevel){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSNumber *lastLevel = [defaults objectForKey:@"lastLevel"];
        self.lastLevel = lastLevel;
    }
    
    if(self.lastLevel){
        
        if(self.levelIndex==1){
            self.levelIndex = [self.lastLevel integerValue];
        }else{
            self.levelIndex = 1;
        }
        [self.gameSceneDelegate changeHomeViewLevel:self.levelIndex];
        [self initCurrentLevel];
        
    }else{
        return;
    }
    
    



}

-(void)initNextLevel{
     NSLog(@"initNextLevel");
    self.levelIndex++;
    
    if(self.levelIndex>[LevelProvider countOfLevels]){
        self.levelIndex=1;
    }
    
    [self.gameSceneDelegate showHomeViewWithLevel:self.levelIndex];
    
    [self initLayoutOfLevel:self.levelIndex];
    
}

-(void)initCurrentLevel{
    NSLog(@"initCurrentLevel");
    [self initLayoutOfLevel:self.levelIndex];
    
}


-(void)initLayoutOfLevel:(NSInteger)levelIndex{
    
    NSLog(@"initLayoutOfLevel: %ld", (long)levelIndex);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:[NSNumber numberWithInteger:levelIndex] forKey:@"lastLevel"];
    
    [defaults synchronize];
    
    self.isStickFlying = NO;
    self.sticksCount = 3;
    [self removeAllBalls];
    
    SKSpriteNode *bgImage = [SKSpriteNode spriteNodeWithImageNamed:[NSString stringWithFormat:@"background%ld.jpg",(long)(self.levelIndex/10.0)+1]];
    bgImage.scale = 1.6;
    bgImage.zPosition = -1.0;
    bgImage.position = CGPointMake(self.size.width/2, self.size.height/2);
    bgImage.name = @"background";
    
    [self addChild:bgImage];
        
    [self putinNewStick];
    [self addStickSigns];
    self.levelLabel.text = [NSString stringWithFormat:@"Level %ld",(long)levelIndex];
    
    NSArray *level = [LevelProvider levelOfIndex:levelIndex];
    self.ballsCount = 0;
    for(int j=1;j<=5;j++){
        for(int i=1;i<=10;i++){
            
            NSInteger ballIndex = [[[level objectAtIndex:(j-1)] objectAtIndex:(i-1)] integerValue];
            
            if(ballIndex>0){
                SKSpriteNode *ball = [self ballWithIndex:ballIndex];
                
                ball.position = CGPointMake(330+40*(i-1),
                                            CGRectGetMaxY(self.frame)-(50+j*40));
                
                [self addChild:ball];
                self.ballsCount++;
            }
            
        }
    }
   

}

-(void)addStickSigns{

    NSArray *sprites = [self children];
    
    for(SKSpriteNode *sprite in sprites){
        if([sprite.name isEqualToString:@"sticksign1"] || [sprite.name isEqualToString:@"sticksign2"]){
            
            [sprite removeFromParent];
            
        }
        
    }
    
    
    SKSpriteNode *sprite1 = [SKSpriteNode spriteNodeWithImageNamed:@"stick1"];
    sprite1.scale = 0.25;
    sprite1.name = @"sticksign1";
    sprite1.position =  CGPointMake(320+2,CGRectGetMinY(self.frame)+22);
    SKAction *action = [SKAction rotateByAngle:-M_PI_4 duration:0];
    [sprite1 runAction:action];
    [self addChild:sprite1];
    
    SKSpriteNode *sprite2 = [SKSpriteNode spriteNodeWithImageNamed:@"stick1"];
    sprite2.name = @"sticksign2";
    sprite2.scale = 0.25;
    sprite2.position =  CGPointMake(320+32,CGRectGetMinY(self.frame)+22);
    [sprite2 runAction:action];

    [self addChild:sprite2];


}

-(void)removeAllBalls{

    
    NSArray *sprites = [self children];
    
    for(SKSpriteNode *sprite in sprites){
        if([sprite.name isEqualToString:@"ball"]){
            
            [sprite removeFromParent];
            
        }
        
        if([sprite.name isEqualToString:@"stick"]){
            
            [sprite removeFromParent];
            
        }
        
        if([sprite.name isEqualToString:@"background"]){
            
            [sprite removeFromParent];
            
        }
    
    }


}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    if(self.isHidden){
        return;
    }
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    //if fire button touched, bring the rain
    if ([node.name isEqualToString:@"menuButtonNode"]) {
        
        
        [self.gameSceneDelegate showHomeViewWithLevel:self.levelIndex];
        
        [self initLayoutOfLevel:self.levelIndex];
        
        return;
    
    }
    
    
    
    if(self.isStickFlying){
        return;
    }
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        NSLog(@"location x: %f y: %f",location.x,location.y);
        
        /*
        SKShapeNode *yourline = [SKShapeNode node];
        CGMutablePathRef pathToDraw = CGPathCreateMutable();
        CGPathMoveToPoint(pathToDraw, NULL, CGRectGetMidX(self.frame),
                          CGRectGetMinY(self.frame)+100);
        CGPathAddLineToPoint(pathToDraw, NULL, location.x, location.y);
        yourline.path = pathToDraw;
        [yourline setStrokeColor:[UIColor redColor]];
        [self addChild:yourline];
        */
        
        CGFloat x = location.x-CGRectGetMidX(self.frame);
        CGFloat y = location.y-100;
        
        NSLog(@"x: %f y: %f",x,y);
        if(y<0){
            return;
        }
        self.isStickFlying = YES;
        
        SKSpriteNode *sprite = (SKSpriteNode *)[self childNodeWithName:@"stick"];
        
        if(sprite.physicsBody.velocity.dx>0.0 || sprite.physicsBody.velocity.dy>0.0){
            return;
        }
        
        if(sprite){
            self.levelIsStarted=NO;
        }
        
        self.lastLevel = nil;
        //SKAction *action = [SKAction moveTo:location duration:2];
        
        //[sprite runAction:action];
        
        CGFloat angleTan = y/x;
        
        CGFloat vX = 45*cos(atan(angleTan));
        CGFloat vY = fabs(45*sin(atan(angleTan)));
        if(x<0){
            vX=-vX;
        }
        NSLog(@"vX: %f vY: %f",vX,vY);
        
        [sprite.physicsBody applyImpulse:CGVectorMake(vX, vY)];
        
        /*
        SKShapeNode *yourline1 = [SKShapeNode node];
        CGMutablePathRef pathToDraw1 = CGPathCreateMutable();
        CGPathMoveToPoint(pathToDraw1, NULL, CGRectGetMidX(self.frame),
                          CGRectGetMinY(self.frame)+100);
        CGPathAddLineToPoint(pathToDraw1, NULL, CGRectGetMidX(self.frame)+vX,CGRectGetMinY(self.frame)+100+vY);
        yourline1.path = pathToDraw1;
        [yourline1 setStrokeColor:[UIColor greenColor]];
        [self addChild:yourline1];
        */
        
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    NSArray *sprites = [self children];
    
    for(SKSpriteNode *sprite in sprites){
        
        CGPoint stickPosition = sprite.position;
        
        //NSLog(@"stickPosition %@  vX: %f vY: %f",sprite.name,stickPosition.x,stickPosition.y);
    
        if([sprite.name isEqualToString:@"stick"]){
            
             if([self isStickOutOfFrame:stickPosition]){
                 self.isStickFlying = NO;
                 self.sticksCount--;
                 if(self.sticksCount==0){
                     [self performSelector:@selector(checkBallsState) withObject:nil afterDelay:2.0];
                 }else{
                     [self putinNewStick];
                     if(self.sticksCount==1){
                         SKSpriteNode *stickSign1 = [sprites filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name = 'sticksign1'"]].lastObject;
                         [stickSign1 removeFromParent];
                     }else if(self.sticksCount==2){
                         SKSpriteNode *stickSign2 = [sprites filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name = 'sticksign2'"]].lastObject;
                         [stickSign2 removeFromParent];
                     }
                     
                 }
                 
                [sprite removeFromParent];
                
                NSLog(@"stick isOutOfFrame");

             }
        }else if([sprite.name isEqualToString:@"ball"]){
            
            float velocity = sqrtf(powf(sprite.physicsBody.velocity.dx,2)+ powf(sprite.physicsBody.velocity.dy,2));
            if(velocity>0.0 && velocity<15.0){
                [sprite.physicsBody setVelocity:CGVectorMake(0, 0)];
                //NSLog(@"Stop");
            }
            
            //NSLog(@"%f",velocity);
            
            if([self isBallOutOfFrame:stickPosition]){
                [sprite removeFromParent];
                 NSLog(@"ball isOutOfFrame");
                self.ballsCount --;
                if(self.ballsCount==0){
                    
                    [self checkBallsState];
                    
                }
                
            }
            
            
           
        }
        
        
    }
    
    
    
}


-(void)checkBallsState{
    
    NSLog(@"self.ballsCount: %ld self.levelIsStarted: %i",(long)self.ballsCount, self.levelIsStarted);
    
    if(!self.levelIsStarted){
        if(self.ballsCount==0){
            [self performSelector:@selector(initNextLevel) withObject:nil afterDelay:2.0];
            self.levelIsStarted = YES;
            return;
        }else{
            
            [self initCurrentLevel];
        }
    }else{
        self.levelIsStarted=NO;
    }

}

-(void)putinNewStick{


    SKSpriteNode *newSprite = [self stickWithIndex:(int)(self.levelIndex/10.0)+1];
    newSprite.position = CGPointMake(CGRectGetMidX(self.frame),
                                     CGRectGetMinY(self.frame)+100);
    SKAction *action = [SKAction rotateByAngle:M_PI duration:1];
    [newSprite runAction:[SKAction repeatActionForever:action]];
    
    [self addChild:newSprite];


}


-(SKSpriteNode *)ballWithIndex:(NSInteger)ballIndex{


    SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:[NSString stringWithFormat:@"ball%ld",(long)ballIndex]];
    sprite.name = @"ball";
    sprite.physicsBody =[SKPhysicsBody bodyWithCircleOfRadius:sprite.size.width/2];
    sprite.physicsBody.dynamic = YES;
    sprite.physicsBody.mass=0.001;
    sprite.physicsBody.friction = 0.0;
    sprite.physicsBody.restitution=0.0;
    sprite.physicsBody.linearDamping=0.9;
    sprite.physicsBody.angularDamping=0.9;

    return sprite;
    
    
}

-(SKSpriteNode *)stickWithIndex:(NSInteger)stickIndex{
    
    SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:[NSString stringWithFormat:@"stick%ld",(long)stickIndex]];
    sprite.name = @"stick";
    
    sprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:sprite.texture.size];
    sprite.physicsBody.dynamic = YES;
    sprite.physicsBody.mass = 0.1;
    sprite.physicsBody.linearDamping=0.0;
    sprite.physicsBody.angularDamping=0.0;
    
    return sprite;
    
}

-(BOOL)isBallOutOfFrame:(CGPoint)point{
    
    if(point.x<300 || point.x>720 || point.y<0 || point.y>760){
        
        return YES;
        
    }

    return NO;
}

-(BOOL)isStickOutOfFrame:(CGPoint)point{
    
    if(point.x<200 || point.x>800 || point.y<0 || point.y>840){
        
        return YES;
        
    }
    
    return NO;
}


@end
