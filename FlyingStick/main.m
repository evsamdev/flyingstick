//
//  main.m
//  FlyingStick
//
//  Created by Evgeny on 4/10/2014.
//  Copyright (c) 2014 EvSamsonov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
