//
//  HomeViewController.m
//  FlyingStick
//
//  Created by Evgeny on 10/10/2014.
//  Copyright (c) 2014 EvSamsonov. All rights reserved.
//

#import "HomeViewController.h"


@interface HomeViewController ()

@property (weak, nonatomic) IBOutlet UIButton *playButton;

- (IBAction)play:(UIButton *)sender;


@property (weak, nonatomic) IBOutlet UIButton *levelButton;
- (IBAction)resetLevel:(UIButton *)sender;


@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Replace this ad unit ID with your own ad unit ID.
    self.bannerView.adUnitID = @"ca-app-pub-3892671015535082/7965826457";
    self.bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    // Requests test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made.
    request.testDevices = @[ GAD_SIMULATOR_ID, @"7d7732eb2fb7561dc48ad53b0171a8ed",@"fb6c8c6bf0ed62c3d19300e2b5586dd1"];
    [self.bannerView loadRequest:request];
    self.titleLabel.font = [UIFont fontWithName:@"Maddac" size:48];
    self.titleLabel.text = [self.titleLabel.text uppercaseString];
    
    self.playButton.titleLabel.font = [UIFont fontWithName:@"Maddac" size:40];
    self.levelButton.titleLabel.font = [UIFont fontWithName:@"Maddac" size:40];
    
    NSLog(@"%@",[UIFont fontNamesForFamilyName:@"Maddac"]);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)play:(UIButton *)sender {
    
    [[self delegate] hideHomeView];
    
}

-(void)showLevel:(NSInteger)level{
     [self.levelButton setTitle:[NSString stringWithFormat:@"Level %ld",(long)level] forState:UIControlStateNormal];

}
- (IBAction)resetLevel:(UIButton *)sender {
    
    [[self delegate] resetLevel];
    
    
}
@end
