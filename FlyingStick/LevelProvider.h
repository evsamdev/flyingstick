//
//  LevelProvider.h
//  FlyingStick
//
//  Created by Evgeny on 26/10/2014.
//  Copyright (c) 2014 EvSamsonov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LevelProvider : NSObject

+(NSArray *)levelOfIndex:(NSInteger)levelIndex;
+(NSInteger)countOfLevels;
+(NSArray *)allLevels;
@end
