//
//  LevelProvider.m
//  FlyingStick
//
//  Created by Evgeny on 26/10/2014.
//  Copyright (c) 2014 EvSamsonov. All rights reserved.
//

#import "LevelProvider.h"

@implementation LevelProvider


+(NSArray *)levelOfIndex:(NSInteger)levelIndex{
    NSArray *level;
    switch (levelIndex) {
        case 1:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@1,@1,@0,@3,@3,@0,@1,@1,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 2:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@1,@1,@0,@0,@2,@2,@0,@0,@1,@1],
                    @[@0,@1,@0,@0,@2,@2,@0,@0,@1,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 3:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@2,@0,@3,@0,@4,@4,@0,@3,@0,@2],
                    @[@0,@1,@0,@0,@4,@4,@0,@0,@1,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 4:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@1,@0,@0,@4,@4,@0,@0,@1,@0],
                    @[@2,@0,@3,@0,@4,@4,@0,@3,@0,@2],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 5:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@1,@0,@0,@3,@3,@0,@0,@1,@0],
                    @[@0,@1,@0,@0,@3,@3,@0,@0,@1,@0],
                    @[@0,@1,@0,@0,@3,@3,@0,@0,@1,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 6:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@2,@0,@0,@0,@0,@0,@0,@6,@0],
                    @[@2,@0,@2,@0,@4,@4,@0,@6,@0,@6],
                    @[@0,@2,@0,@0,@0,@0,@0,@0,@6,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 7:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@5,@2,@5,@0,@4,@4,@0,@6,@3,@6],
                    @[@0,@5,@0,@0,@4,@4,@0,@0,@6,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 8:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@2,@0,@0,@1,@1,@0,@0,@6,@0],
                    @[@2,@3,@2,@0,@1,@1,@0,@6,@3,@6],
                    @[@0,@2,@0,@0,@1,@1,@0,@0,@6,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 9:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@4,@0,@4,@0,@2,@2,@0,@3,@0,@3],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@4,@0,@4,@0,@2,@2,@0,@3,@0,@3],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 10:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@2,@0,@3,@0,@4,@4,@0,@3,@0,@2],
                    @[@0,@1,@0,@0,@0,@0,@0,@0,@1,@0],
                    @[@2,@0,@3,@0,@4,@4,@0,@3,@0,@2],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 11:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@1,@1,@1,@3,@3,@3,@3,@5,@5,@5]
                    ];
            break;
        case 12:
            level=@[@[@2,@0,@0,@3,@0,@0,@6,@0,@0,@0],
                    @[@0,@2,@0,@0,@3,@0,@0,@6,@0,@0],
                    @[@0,@0,@2,@0,@0,@3,@0,@0,@6,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 13:
            level=@[@[@0,@0,@0,@1,@0,@0,@4,@0,@0,@5],
                    @[@0,@0,@1,@0,@0,@4,@0,@0,@5,@0],
                    @[@0,@1,@0,@0,@4,@0,@0,@5,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 14:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@2,@3,@0,@0,@0,@0,@4,@0],
                    @[@0,@2,@0,@0,@3,@0,@0,@4,@0,@0],
                    @[@2,@0,@0,@0,@0,@3,@4,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 15:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@5,@0,@0,@0,@0,@1,@6,@0,@0],
                    @[@0,@0,@5,@0,@0,@1,@0,@0,@6,@0],
                    @[@0,@0,@0,@5,@1,@0,@0,@0,@0,@6],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 16:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@2,@0,@2,@0,@2,@0,@2,@0,@2],
                    @[@1,@0,@1,@0,@1,@0,@1,@0,@1,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 17:
            level=@[@[@1,@0,@0,@0,@0,@0,@0,@0,@0,@4],
                    @[@0,@1,@0,@0,@0,@0,@0,@0,@4,@0],
                    @[@0,@0,@1,@0,@0,@0,@0,@4,@0,@0],
                    @[@0,@0,@0,@1,@0,@0,@4,@0,@0,@0],
                    @[@0,@0,@0,@0,@1,@4,@0,@0,@0,@0]
                    ];
            break;
        case 18:
            level=@[@[@0,@0,@0,@0,@2,@3,@0,@0,@0,@0],
                    @[@0,@0,@0,@2,@0,@0,@3,@0,@0,@0],
                    @[@0,@0,@2,@0,@0,@0,@0,@3,@0,@0],
                    @[@0,@2,@0,@0,@0,@0,@0,@0,@3,@0],
                    @[@2,@0,@0,@0,@0,@0,@0,@0,@0,@3]
                    ];
            break;
        case 19:
            level=@[@[@0,@1,@0,@1,@0,@0,@2,@0,@2,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@5,@0,@0,@0,@5,@3,@0,@0,@0,@3],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@4,@0,@4,@0,@0,@6,@0,@6,@0]
                    ];
            break;
        case 20:
            level=@[@[@0,@2,@0,@0,@5,@5,@0,@0,@4,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@1,@0,@1,@0,@0,@0,@0,@3,@0,@3],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@1,@0,@1,@0,@5,@5,@0,@3,@0,@3]
                    ];
            break;
        case 21:
            level=@[@[@0,@0,@0,@0,@3,@4,@0,@0,@0,@0],
                    @[@0,@0,@0,@2,@3,@4,@5,@0,@0,@0],
                    @[@0,@0,@2,@0,@3,@4,@0,@5,@0,@0],
                    @[@0,@2,@0,@0,@3,@4,@0,@0,@5,@0],
                    @[@2,@0,@0,@0,@3,@4,@0,@0,@0,@5]
                    ];
            break;
        case 22:
            level=@[@[@3,@0,@0,@0,@5,@6,@0,@0,@0,@1],
                    @[@0,@3,@0,@0,@5,@6,@0,@0,@1,@0],
                    @[@0,@0,@3,@0,@5,@6,@0,@1,@0,@0],
                    @[@0,@0,@0,@3,@5,@6,@1,@0,@0,@0],
                    @[@0,@0,@0,@0,@5,@6,@0,@0,@0,@0]
                    ];
            break;
        case 23:
            level=@[@[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@4,@4,@5,@5,@6,@6,@1,@1,@2,@2],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@4,@4,@5,@5,@6,@6,@1,@1,@2,@2],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0]
                    ];
            break;
        case 24:
            level=@[@[@0,@0,@0,@0,@2,@2,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@2,@2,@0,@0,@0,@0],
                    @[@3,@3,@3,@3,@1,@1,@4,@4,@4,@4],
                    @[@0,@0,@0,@0,@2,@2,@0,@0,@0,@0],
                    @[@0,@0,@0,@0,@2,@2,@0,@0,@0,@0]
                    ];
            break;
        case 25:
            level=@[@[@4,@0,@0,@0,@0,@0,@0,@0,@0,@5],
                    @[@0,@4,@0,@0,@3,@3,@0,@0,@5,@0],
                    @[@0,@0,@4,@4,@6,@6,@5,@5,@0,@0],
                    @[@0,@4,@0,@0,@3,@3,@0,@0,@5,@0],
                    @[@4,@0,@0,@0,@0,@0,@0,@0,@0,@5]
                    ];
            break;
        case 26:
            level=@[@[@1,@0,@1,@0,@2,@2,@0,@4,@0,@4],
                    @[@1,@0,@1,@0,@2,@2,@0,@4,@0,@4],
                    @[@1,@3,@1,@4,@2,@2,@5,@4,@6,@4],
                    @[@1,@0,@1,@0,@2,@2,@0,@4,@0,@4],
                    @[@1,@0,@1,@0,@2,@2,@0,@4,@0,@4]
                    ];
            break;
        case 27:
            level=@[@[@0,@2,@0,@0,@4,@4,@0,@0,@6,@0],
                    @[@2,@0,@2,@3,@0,@0,@3,@6,@0,@6],
                    @[@0,@4,@0,@0,@0,@0,@0,@0,@5,@0],
                    @[@2,@0,@2,@3,@0,@0,@3,@6,@0,@6],
                    @[@0,@2,@0,@0,@4,@4,@0,@0,@6,@0]
                    ];
            break;
        case 28:
            level=@[@[@3,@3,@3,@3,@0,@0,@4,@4,@4,@4],
                    @[@3,@0,@0,@3,@0,@0,@4,@0,@0,@4],
                    @[@3,@0,@0,@3,@5,@5,@4,@0,@0,@4],
                    @[@3,@0,@0,@3,@0,@0,@4,@0,@0,@4],
                    @[@3,@3,@3,@3,@0,@0,@4,@4,@4,@4]
                    ];
            break;
        case 29:
            level=@[@[@4,@0,@0,@0,@5,@1,@0,@0,@0,@6],
                    @[@0,@4,@0,@5,@0,@0,@1,@0,@6,@0],
                    @[@0,@0,@3,@0,@0,@0,@0,@2,@0,@0],
                    @[@0,@5,@0,@4,@0,@0,@6,@0,@1,@0],
                    @[@5,@0,@0,@0,@4,@6,@0,@0,@0,@1]
                    ];
            break;
        case 30:
            level=@[@[@1,@0,@0,@0,@3,@4,@0,@0,@0,@6],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@0,@0,@2,@0,@0,@0,@0,@5,@0,@0],
                    @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0],
                    @[@1,@0,@0,@0,@3,@4,@0,@0,@0,@6]
                    ];
            break;
        case 31:
            level=@[@[@0,@0,@4,@0,@0,@0,@0,@5,@0,@0],
                    @[@0,@4,@0,@4,@0,@0,@5,@0,@5,@0],
                    @[@4,@0,@3,@0,@4,@5,@0,@1,@0,@5],
                    @[@0,@4,@0,@4,@0,@0,@5,@0,@5,@0],
                    @[@0,@0,@4,@0,@0,@0,@0,@5,@0,@0]
                    ];
            break;
        case 32:
            level=@[@[@1,@0,@1,@0,@3,@3,@3,@5,@0,@5],
                    @[@1,@0,@1,@0,@3,@0,@0,@5,@0,@5],
                    @[@1,@1,@1,@0,@3,@3,@0,@0,@5,@0],
                    @[@1,@0,@1,@0,@3,@0,@0,@0,@5,@0],
                    @[@1,@0,@1,@0,@3,@3,@3,@0,@5,@0]
                    ];
            break;
        case 33:
            level=@[@[@2,@0,@0,@0,@3,@4,@0,@0,@0,@5],
                    @[@0,@2,@0,@3,@0,@0,@4,@0,@5,@0],
                    @[@0,@0,@3,@0,@0,@0,@0,@4,@0,@0],
                    @[@0,@3,@0,@2,@0,@0,@5,@0,@4,@0],
                    @[@3,@0,@0,@0,@2,@5,@0,@0,@0,@4]
                    ];
            break;
        case 34:
            level=@[@[@0,@0,@0,@0,@3,@5,@0,@0,@0,@0],
                    @[@0,@0,@0,@3,@0,@0,@5,@0,@0,@0],
                    @[@3,@0,@3,@0,@0,@0,@0,@5,@0,@5],
                    @[@0,@3,@6,@0,@0,@0,@0,@6,@5,@0],
                    @[@0,@0,@0,@6,@6,@6,@6,@0,@0,@0]
                    ];
            break;
        case 35:
            level=@[@[@0,@6,@0,@6,@0,@6,@0,@6,@0,@6],
                    @[@3,@0,@3,@0,@3,@0,@3,@0,@3,@0],
                    @[@0,@5,@0,@5,@0,@5,@0,@5,@0,@5],
                    @[@4,@0,@4,@0,@4,@0,@4,@0,@4,@0],
                    @[@0,@6,@0,@6,@0,@6,@0,@6,@0,@6]
                    ];
            break;
        case 36:
            level=@[@[@6,@0,@0,@0,@0,@0,@0,@0,@0,@2],
                    @[@3,@6,@0,@0,@4,@4,@0,@0,@2,@1],
                    @[@3,@0,@6,@4,@0,@0,@4,@2,@0,@1],
                    @[@3,@6,@0,@0,@4,@4,@0,@0,@2,@1],
                    @[@6,@0,@0,@0,@0,@0,@0,@0,@0,@2]
                    ];
            break;
        case 37:
            level=@[@[@0,@0,@0,@5,@5,@5,@5,@0,@0,@0],
                    @[@3,@0,@0,@5,@0,@0,@5,@0,@0,@4],
                    @[@0,@3,@0,@5,@0,@0,@5,@0,@4,@0],
                    @[@0,@0,@3,@5,@0,@0,@5,@4,@0,@0],
                    @[@0,@0,@0,@5,@5,@5,@5,@0,@0,@0]
                    ];
            break;
        case 38:
            level=@[@[@0,@0,@0,@6,@6,@6,@6,@0,@0,@0],
                    @[@0,@0,@4,@6,@0,@0,@6,@3,@0,@0],
                    @[@0,@4,@0,@6,@0,@0,@6,@0,@3,@0],
                    @[@4,@0,@0,@6,@0,@0,@6,@0,@0,@3],
                    @[@0,@0,@0,@6,@6,@6,@6,@0,@0,@0]
                    ];
            break;
        case 39:
            level=@[@[@1,@4,@4,@4,@4,@4,@4,@4,@4,@3],
                    @[@1,@0,@5,@0,@0,@0,@0,@6,@0,@3],
                    @[@1,@0,@0,@5,@0,@0,@6,@0,@0,@3],
                    @[@1,@0,@5,@0,@0,@0,@0,@6,@0,@3],
                    @[@1,@2,@2,@2,@2,@2,@2,@2,@2,@3]
                    ];
            break;
            
        default:
            break;
    }

    
    return level;
}

+(NSInteger)countOfLevels{

    return 39;
}

+(NSArray *)allLevels{

    NSMutableArray *levels = [[NSMutableArray alloc] init];
    
    for (int i=1; i<=[LevelProvider countOfLevels]; i++) {
        
        NSArray *level = [LevelProvider levelOfIndex:i];
        [levels addObject:level];
        
    }
    

    return levels;
}

@end
