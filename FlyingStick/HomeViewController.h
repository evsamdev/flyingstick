//
//  HomeViewController.h
//  FlyingStick
//
//  Created by Evgeny on 10/10/2014.
//  Copyright (c) 2014 EvSamsonov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"
#import "GADRequest.h"

@protocol HomeViewControllerDelegate <NSObject>

-(void)hideHomeView;
-(void)resetLevel;

@end


@interface HomeViewController : UIViewController
@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;
@property(nonatomic, weak) id<HomeViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
-(void)showLevel:(NSInteger)level;

@end
