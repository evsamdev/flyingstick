//
//  AppDelegate.h
//  FlyingStick
//
//  Created by Evgeny on 4/10/2014.
//  Copyright (c) 2014 EvSamsonov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

