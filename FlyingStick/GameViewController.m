//
//  GameViewController.m
//  FlyingStick
//
//  Created by Evgeny on 4/10/2014.
//  Copyright (c) 2014 EvSamsonov. All rights reserved.
//

#import "GameViewController.h"
#import "LevelProvider.h"


@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    HomeViewController* vc = (HomeViewController *)[sb instantiateViewControllerWithIdentifier:@"HomeViewController"];
    vc.delegate = self;
    
    self.homeViewController = vc;
    
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    //skView.showsFPS = YES;
    //skView.showsNodeCount = YES;
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    skView.ignoresSiblingOrder = YES;
    
    // Create and configure the scene.
    GameScene *scene =[GameScene unarchiveFromFile:@"GameScene"];
    
    
    scene.scaleMode = SKSceneScaleModeAspectFill;
    scene.gameSceneDelegate = self;
    self.scene = scene;
    // Present the scene.
    [skView presentScene:scene];
    
    /*
    NSArray *levels = [LevelProvider allLevels];
    
    NSData *levelsData = [NSJSONSerialization dataWithJSONObject:levels options:NSJSONWritingPrettyPrinted error:nil];
    
    NSString *levelsJson = [[NSString alloc] initWithData:levelsData encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",levelsJson);
    */
    
    
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)hideHomeView{
     //[self.homeViewController.view removeFromSuperview];
    
    [UIView animateWithDuration:0.4 animations:^{
        self.homeViewController.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.homeViewController.view removeFromSuperview];
        self.homeViewController.view.alpha = 1.0;
        self.scene.isHidden = NO;
    }];
    
    
   
    
}

-(void)showHomeViewWithLevel:(NSInteger)level{
    NSLog(@"showHomeView with Level %ld",(long)level);
    [self.view addSubview:self.homeViewController.view];
    [self.homeViewController showLevel:level];
    self.scene.isHidden = YES;
    /*
    self.homeViewController.view.alpha = 0.0;
    [self.view addSubview:self.homeViewController.view];
    
    [UIView animateWithDuration:0.7 animations:^{
        self.homeViewController.view.alpha = 1.0;
    } completion:^(BOOL finished) {
        
    }];
    */
    
    
}

-(void)changeHomeViewLevel:(NSInteger)level{
    [self.homeViewController showLevel:level];

}

-(void)resetLevel{

    [self.scene resetLevel];

}

@end
