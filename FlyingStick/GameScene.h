//
//  GameScene.h
//  FlyingStick
//

//  Copyright (c) 2014 EvSamsonov. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@protocol GameSceneDelegate <NSObject>

-(void)showHomeViewWithLevel:(NSInteger)level;
-(void)changeHomeViewLevel:(NSInteger)level;
@end

@interface GameScene : SKScene

@property (weak, nonatomic) id<GameSceneDelegate> gameSceneDelegate;
@property (nonatomic) BOOL isHidden;
-(void)resetLevel;

@end
